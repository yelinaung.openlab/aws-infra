# Infrastructure As Code (IaC)

This project uses Infrastructure as Code (IaC) to provision the necessary infrastructure on AWS. The project uses CloudFormation to create a network, security group, and node. This ensures that the infrastructure is created consistently and can be easily reproduced.
<br />
<br />

# Deployment Process
The deployment process is automated using GitLab CI/CD pipelines. The pipeline checks if the CloudFormation stack already exists for each stage. If the stack exists, the pipeline does not do anything and exits. If the stack does not exist, the pipeline creates the stack and waits for the stack creation to complete.
<br />

During stack creation, the pipeline continuously checks the stack status until it is either successfully created or fails. If the stack is successfully created, the pipeline exits with a success status. If the stack creation fails, the pipeline exits with a failure status and logs the error message.
<br />
<br />

# Usage
00-network.yaml file will create required network infra included VPC, Public Subnet, Private Subnet, IGW and Route.

01-security-group.yaml file will create Security Group in created subnet for Node.

02-node.yaml file will create EC2 instance in created subnet to run RKE.

Exposed required changable value as a parameter in parameter.json file.

To use this project, simply clone the repository and follow the instructions in the README.md file. Before deploying the project, make sure to set up the necessary environment variables and credentials for accessing AWS.


<br />
<br />

# Contributing
Contributions to this project are welcome! To contribute, simply fork the repository and create a pull request with your changes.
<br />
<br />

# Authors
This project was created by maintained by `Ye Lin Aung`.
